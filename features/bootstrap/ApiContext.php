<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class ApiContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }

    /**
     * @Given I have to :todo
     */
    public function iHaveTodo($todo)
    {
        throw new PendingException();
    }

    /**
     * @Given I do not have to :todo
     */
    public function iDoNotHaveTodo($todo)
    {
        throw new PendingException();
    }

    /**
     * @Given I have to :todo in my todo list
     */
    public function iHaveToInMyTodoList($todo)
    {
        throw new PendingException();
    }

    /**
     * @When I describe this todo
     * @When I describe it again
     */
    public function iDescribeThisTodo()
    {
        throw new PendingException();
    }

    /**
     * @Then I should see the todo in the list
     */
    public function iShouldSeeTheTodoInTheList()
    {
        throw new PendingException();
    }

    /**
     * @Then I should be informed of an error
     */
    public function iShouldBeInformedOfAnError()
    {
        throw new PendingException();
    }

    /**
     * @Then I should see the todo :count in the list
     */
    public function iShouldSeeTheTodoNthTimesInTheList($count)
    {
        throw new PendingException();
    }

    /**
     * @Given I completed this todo
     */
    public function iCompletedThisTodo()
    {
        throw new PendingException();
    }

    /**
     * @Then I should not see the todo in the list
     */
    public function iShouldNotSeeTheTodoInTheList()
    {
        throw new PendingException();
    }

    /**
     * @When I complete this todo
     */
    public function iCompleteThisTodo()
    {
        throw new PendingException();
    }

    /**
     * @Then I should see the todo as completed in the list
     */
    public function iShouldSeeTheTodoAsCompletedInTheList()
    {
        throw new PendingException();
    }

    /**
     * @When I complete again this todo
     */
    public function iCompleteAgainThisTodo()
    {
        throw new PendingException();
    }
}
